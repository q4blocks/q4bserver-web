FROM nginx
RUN rm /etc/nginx/conf.d/default.conf && mkdir -p /usr/share/nginx/html/web && mkdir -p /usr/share/nginx/html/editor
COPY nginx-conf/default.conf /etc/nginx/conf.d
COPY web /usr/share/nginx/html/web
COPY editor /usr/share/nginx/html/editor
